/*
  Reaction
 
 A reaction game, tracking the fastest reaction so far.

 User presses a button to start, and then after a random delay, is told to press the button again.
 The faster they press the button again, the better their score!
 If the player presses the button too fast, they lose!


  Code Layout
  1. ARDUINO MANAGEMENT FUNCTIONS
  2. GAME STATE MANAGEMENT FUNCTIONS
  3. DISPLAY DRAWING FUNCTIONS
  4. MEMORY MANAGEMENT FUNCTIONS

 */


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Game modes
#define HOME_SCREEN 0
#define GAME_START_DELAY 1
#define WAITING_FOR_PRESS 2
#define GAME_FAILED 3
#define GAME_OVER 4

#define LED 13
#define BUTTON 2
#define I2C_DISPLAY  0x3C // i2c display's address

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// current game mode
int mode = HOME_SCREEN;
int previous_mode = -1;

// Min and max delays for the waiting period
long minDelay = 3;  // Seconds
long maxDelay = 12; // Seconds

// Actual delay of current game
long random_delay = 0;

// When the current game started
unsigned long current_game_start = 0; // When did the user start this game?
unsigned long current_game_end = 0;   // When did the user hit the button to end the game?
unsigned long user_reaction_start = 0;


// User's score. Lower is better
long score = 0;
int best_score = 0; // Best score. This is loaded from eeprom into this variable when program started
int best_score_address = 0; // EEPROM address for long term storage of the best score

long debounce_delay = 1000; // Debouncing time in milliseconds
long last_button_press = 0; // Last time button was pressed. Used to debounce

/*
 * ==== 1. ARDUINO MANAGEMENT FUNCTIONS ==== 
 * 
 */

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT);
  Serial.begin(115200);
  Serial.println("Time to react");


  if (digitalRead(BUTTON) == HIGH){
    /* If button is pressed when starting up, reset the score */
    EEPROMWriteInt(best_score_address, 0);
      for (int i=0; i< 10; i++){
        // Flash a couple of times to let user know it was reset
        digitalWrite(LED, HIGH);
        delay(100);
        digitalWrite(LED, LOW);
        delay(100);
      }
  }

  // Read best score from EEPROM
  best_score = EEPROMReadInt(best_score_address);
  Serial.print("Loaded previous best score of ");
  Serial.println(best_score);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  for (int i=0; i< 3; i++){
    // Flash a couple of times to get started. This delay is also needed to ensure the screen starts up
    digitalWrite(LED, HIGH);
    delay(1000);
    digitalWrite(LED, LOW);
    delay(1000);
  }
  if(!display.begin(SSD1306_SWITCHCAPVCC, I2C_DISPLAY)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  Serial.println("Starting up");
  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  drawCircles();

  // Clear the buffer
  display.clearDisplay();
}


// the loop routine runs over and over again forever:
void loop() {
  /* Game Flow:
   *  
   *  HOME_SCREEN: Game ready to go, the "home" screen. no active game in progress
   *  - Press button to go to GAME_START_DELAY
   *  
   *  GAME_START_DELAY: Game is started, in the "random delay" stage
   *  - If user presses button, game is lost and go to GAME_OVER
   *  - After a random wait, go to WAITING_FOR_PRESS
   *  
   *  WAITING_FOR_PRESS: Random delay finished, user can press button now
   *  - When user presses button, their game is over and go to GAME_OVER
   *  
   *  
   *  GAME_OVER: Game finished, show the user their score or losing message
   *  - If user presses button, go to HOME_SCREEN
   *  
   */
  switch (mode){
    case HOME_SCREEN:
      // The home screen. User presses button to start game
      show_home_screen();
      break;
    case GAME_START_DELAY:
      // The random delay between a user starting the game and being told to hit
      game_start_delay();
      break;
    case WAITING_FOR_PRESS:
      // When the user presses the button now, it will be a legal hit
      legal_game_over();
      break;
    case GAME_OVER:
      // User presses button to start again
      if (check_debounced_hit()){
        mode = HOME_SCREEN;
      }
      break;
    default:
      Serial.println("Unknown state");
      Serial.println(mode);
  }
}

bool check_debounced_hit(){
  /* Checks if the button is high, but debounced */
  if (digitalRead(BUTTON) == HIGH and (millis() - last_button_press) > debounce_delay){
    last_button_press = millis();
    return true;
  }
  return false;
}


/**
 * 
 * ==== 2. GAME STATE MANAGEMENT FUNCTIONS ==== 
 * 
 */

void show_home_screen(){
  digitalWrite(LED, HIGH);

  /* Show start screen to the user if they haven't seen it yet */
  if (!(previous_mode == mode)){
    Serial.println("Press the button to play.");
    Serial.println("A random and hidden countdown will begin.");
    Serial.println("When the light goes on, press the button quickly!");
    drawGameStartScreen();
  }

  /* When user presses button, start the game */
  if (check_debounced_hit()){
    Serial.println("Game started!");
    digitalWrite(LED, LOW); // dim lights...
    mode = GAME_START_DELAY; // Move to next stage...
    random_delay = random(minDelay, maxDelay); // Set delay
    current_game_start = millis();
    drawWaitingScreen();
  }
}


void game_start_delay(){
  // Check if this round is over
  current_game_end = millis();
  if (current_game_end - current_game_start >= random_delay * 1000){
    digitalWrite(LED, HIGH);
    // We are now waiting for the user to press the button
    mode = WAITING_FOR_PRESS;
    drawPushNow();
    user_reaction_start = millis();
  }else if (check_debounced_hit()){
    // If the user pressed the button too early, they lose
    digitalWrite(LED, LOW);
    Serial.println("Too early! You lost!");
    drawLosingScreen();
    mode = GAME_OVER;
  }else{
    // I don't want to wait, for the delay to be over. I want to know right now what will it be
  }
}


void legal_game_over(){
  if (check_debounced_hit()){
    Serial.println("Drawing start screen");
    drawHeader();
    display.setTextSize(1);             // Normal 1:1 pixel scale

    
    Serial.println("Pressed!");
    score = millis() - user_reaction_start;
    Serial.print("Your score is: ");
    Serial.println(score);
    Serial.println("Lower scores are better");
    display.print(F("Your score:"));
    display.setTextSize(1);

    display.println(score);
    
    if ((best_score == 0) || score < best_score){
      Serial.println("That's the new best score!");
      display.println(F("New best score!"));
      best_score = score;
      EEPROMWriteInt(best_score_address, best_score);
    }else{
      Serial.print("Today's best score is:");
      Serial.println(best_score);
      display.print(F("Best Score:"));
      display.println(best_score);
    }
    // Waiting for next game to start
    mode = GAME_OVER;
    display.display();
    current_game_end = millis();
    delay(2000);
  }
}


/**
 * ==== 3. DISPLAY DRAWING FUNCTIONS ==== 
 * 
 */

void drawHeader(){
  /* Draws the header and "logo" */
  display.clearDisplay();
  display.setCursor(0,0);             // Start at top-left corner
  display.setTextColor(SSD1306_WHITE);        // Draw white text

  display.setTextSize(2);
  display.println(F("REACTION"));
}


void drawGameStartScreen(){
  Serial.println("Drawing start screen");
  drawHeader();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.println(F("Push the button!"));
  display.display();
}


void drawPushNow(){
  Serial.println("Drawing push now screen");
  drawHeader();

  display.println(F("NOW!"));
  display.display();
}


void drawWaitingScreen(){
  Serial.println("Drawing waiting screen");
  drawHeader();

  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.println(F("Wait for it..."));
  display.display();
}


void drawLosingScreen(){
  Serial.println("Drawing start screen");
  drawHeader();

  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.println(F("Too quick, you lost!"));
  display.display();
  delay(2000);
}


void drawCircles(void) {
  display.clearDisplay();

  for(int16_t i=0; i<max(display.width(),display.height())/2; i+=2) {
    display.drawCircle(display.width()/2, display.height()/2, i, SSD1306_WHITE);
    display.display();
    delay(1);
  }
  delay(1000);
}



/**
 * ==== 4. MEMORY MANAGEMENT FUNCTIONS ==== 
 * 
 */

//This function will write a 2 byte integer to the eeprom at the specified address and address + 1
void EEPROMWriteInt(int p_address, int p_value){
  byte lowByte = ((p_value >> 0) & 0xFF);
  byte highByte = ((p_value >> 8) & 0xFF);
  
  EEPROM.write(p_address, lowByte);
  EEPROM.write(p_address + 1, highByte);
}


//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int EEPROMReadInt(int p_address){
   byte lowByte = EEPROM.read(p_address);
   byte highByte = EEPROM.read(p_address + 1);
  
   return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}
